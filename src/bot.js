const startup = require('./Handlers/startup');
const get_token = require('./Handlers/get_token');
const { Client } = require('discord.js');

const prefix = "!";

startup();

let client = new Client;
client.login(get_token());

client.on('ready', () => {
    console.log(`Logged in successfully as ${client.user.tag}!`);
})

client.on('message', (message) => {
    
    // checks for if a command is triggered
    if (message.content.startsWith(prefix)) {
        
        if (message.content == prefix + "hello") {
            message.channel.send("Hello there!!!")
        }
    }
})
