const fs = require('fs');

module.exports = function startup() {
    let result = check_data_directory();
    if (result == true) {
        console.log("Could not find the Data directory, made one for you!");
    }
    
    result = check_token_file_exists();
    if (result == true) {
        console.log("Could not find token file, made one for you!");
    }
}

function check_data_directory() {  // Returns true if the Data directory is created, else returns false
    // Checks to see if the Data directory exists
    if (fs.existsSync('./Data') == false) {
        // If the Data directory does not exist, creates one
        fs.mkdir('./Data', function (error) {
            console.log(error);
        })
        return true;
    }
    else {
        return false;
    }
}

function check_token_file_exists() {  // Returns true if token file is created, else returns false
    // Checks to see if the token file exists
    if (fs.existsSync('./Data/token') == false) {
        // If the token file does not exist, creates one
        fs.openSync("./Data/token", 'w');
        return true;
    }
    else {
        return false;
    }
}
